package com.zuitt;

import java.util.ArrayList;


public class Contact {
    private ArrayList<Contact> contact = new ArrayList<Contact>();
    private String name;
    private ArrayList<String> numbers = new ArrayList<String>();
    private ArrayList<String> address = new ArrayList<String>();

    public Contact(){}

    public Contact(String name, ArrayList<String> numbers, ArrayList<String> address){
        this.name = name;
        this.numbers = numbers;
        this.address = address;

    }


    public String getName() {
        return name;
    }


    public ArrayList<String> getNumber() {
        return numbers;
    }

    public ArrayList<String> getAddress(){
        return address;
    }
    public ArrayList<Contact> getContact(){
        return contact;
    }

    public void setName(String name) {
        this.name = name;
//        contact.add(name);
    }

//    @Override
//    public String toString(){
//        return
//                this.name = name;
//    }

    public void setNumber(String newNumber){
        numbers.add(newNumber);
//        contact.add(newNumber);
    }

    public void setAddress(String newAddress){
        address.add(newAddress);
//        contact.add(newAddress);
    }

}
