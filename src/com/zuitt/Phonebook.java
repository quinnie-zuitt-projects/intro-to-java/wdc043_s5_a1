package com.zuitt;

import java.util.ArrayList;

public class Phonebook {
    private ArrayList<String> contacts = new ArrayList<>();

    public Phonebook(){}

    public Phonebook(ArrayList<String> contacts) {
        this.contacts = contacts;
    }

    public ArrayList<String> getContacts(){
        return contacts;
    }
    public void setContacts(String newContacts){
         contacts.add(newContacts);
    }


}
